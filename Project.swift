import ProjectDescription

let infoPlist: [String: InfoPlist.Value] = [
    "UILaunchScreen": [:]
]

let project = Project(
    name: "SnakeGame",
    packages: [
        .package(url: "https://github.com/pointfreeco/swift-composable-architecture.git", .upToNextMajor(from: "0.27.0"))
    ],
    targets: [
        Target(
            name: "SnakeGame",
            platform: .iOS,
            product: .app,
            bundleId: "udev.dev.SnakeGame",
            infoPlist: "Targets/SnakeGame/Sources/Info.plist",
            sources: [
                "Targets/SnakeGame/Sources/**"
            ],
            resources: [
                "Targets/SnakeGame/Resources/**"
            ],
            dependencies: [
                .package(product: "ComposableArchitecture")
            ]
        ),
        Target(name: "SnakeGameTests",
               platform: .iOS,
               product: .unitTests,
               bundleId: "udev.dev.SnakeGameTests",
               infoPlist: "Targets/SnakeGame/Tests/UnitTests/Info.plist",
               sources: [
                    "Targets/SnakeGame/Tests/UnitTests/**",
                    "Targets/SnakeGame/Sources/**"
               ],
               resources: [],
               dependencies: [
                    .package(product: "ComposableArchitecture")
               ]
        ),
        Target(name: "SnakeGameUITests",
               platform: .iOS,
               product: .uiTests,
               bundleId: "udev.dev.SnakeGameUITests",
               infoPlist: "Targets/SnakeGame/Tests/UITests/Info.plist",
               sources: [
                    "Targets/SnakeGame/Tests/UITests/**",
                    "Targets/SnakeGame/Sources/**"
               ],
               resources: [],
               dependencies: [
                    .package(product: "ComposableArchitecture"),
                    .target(name: "SnakeGame"),
               ]
        )
    ]
)
