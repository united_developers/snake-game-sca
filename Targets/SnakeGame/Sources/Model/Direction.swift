import Foundation

enum Direction: Equatable {
    case up, down, left, right
    
    func isOppositeOf(_ direction: Direction) -> Bool {
        switch (self, direction) {
        case (.up, .down), (.down, .up), (.left, .right), (.right, .left):
            return true
        default:
            return false
        }
    }
}
