import Foundation

struct Snake {
    var headPosition: Location
    var tailPosition: [Location]
    var currentDirection: Direction
    
    init(head headPosition: Location, of lenght: Int) {
        self.headPosition = headPosition
        self.tailPosition = []
        self.currentDirection = .right
        self.tailPosition = (1..<lenght)
            .reduce(into: []) {
                $0.append(Location(self.headPosition.row, self.headPosition.column - $1))
            }
    }
    
    var allPositions: Set<Location> {
        var result = tailPosition
        result.append(headPosition)
        return Set<Location>(result)
    }
    
    func canMove(in direction: Direction) -> Bool {
        !direction.isOppositeOf(currentDirection)
    }
    
    func headLocation(in direction: Direction) -> Location {
        switch direction {
        case .up:
            var row = headPosition.row - 1
            row = row < 0 ? 10 : row
            return Location(row, headPosition.column)
        case .down:
            var row = headPosition.row + 1
            row = row > 10 ? 0 : row
            return Location(row, headPosition.column)
        case .left:
            var column = headPosition.column - 1
            column = column < 0 ? 10 : column
            return Location(headPosition.row, column)
        case .right:
            var column = headPosition.column + 1
            column = column > 10 ? 0 : column
            return Location(headPosition.row, column)
        }
    }
    
    func moving(direction: Direction, hasEaten: (Location) -> Bool) -> Snake {
        let head = headLocation(in: direction)
        var snake = self
        if hasEaten(head) {
            snake.tailPosition.insert(snake.headPosition, at: 0)
            snake.headPosition = head
        } else {
            snake.tailPosition = Array(snake.tailPosition.dropLast())
            snake.tailPosition.insert(snake.headPosition, at: 0)
            snake.headPosition = head
        }
        return snake
    }
}

extension Snake: Equatable {
    static func == (lhs: Snake, rhs: Snake) -> Bool {
        lhs.headPosition == rhs.headPosition && lhs.currentDirection == lhs.currentDirection
    }
}
