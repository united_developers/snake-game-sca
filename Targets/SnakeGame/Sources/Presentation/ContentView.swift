import SwiftUI
import ComposableArchitecture

struct ContentView: View {
    var store: Store<AppState, AppAction>
    
    var body: some View {
        WithViewStore(self.store) { (viewStore: ViewStore<AppState, AppAction>) in
            VStack {
                gameField(viewStore: viewStore)
                Spacer()
                controls(viewStore: viewStore)
            }
            .onAppear {
                viewStore.send(.startGame)
            }
            .alert(self.store.scope(state: { $0.alertState }), dismiss: AppAction.startGame)
        }
        
    }
    
    private func gameField(viewStore: ViewStore<AppState, AppAction>) -> some View {
        return VStack(alignment: .center, spacing: 0) {
            ForEach(0..<11) { row in
                return column(for: row, viewStore: viewStore)
            }
        }
        .padding()
        .border(Color.black, width: 2)
    }
    
    private func controls(viewStore: ViewStore<AppState, AppAction>) -> some View {
        return VStack {
            button(with: "🔼", for: .up, viewStore: viewStore)
            HStack {
                button(with: "◀️", for: .left, viewStore: viewStore)
                button(with: "🔽", for: .down, viewStore: viewStore)
                button(with: "▶️", for: .right, viewStore: viewStore)
            }
            .padding(10)
        }
    }
    
    private func button(with text: String, for direction: Direction, viewStore: ViewStore<AppState, AppAction>) -> some View {
        return Button {
            viewStore.send(.changeCurrent(direction))
        } label: {
            Text(text)
                .font(Font(UIFont(name: "Courier", size: 60)!))
        }

    }
    
    private func cell(for location: Location, viewStore: ViewStore<AppState, AppAction>) -> some View {
        let symbol =
            viewStore.mouse == location ? "🐭" :
            viewStore.snake.headPosition == location ? "🟢":
            viewStore.snake.tailPosition.contains(location) ? "⚫️" : " "
        return Text(symbol)
            .font(Font(UIFont(name: "Courier", size: 35)!))
            .frame(width: 35, height: 35, alignment: .center)
    }
    
    private func column(for row: Int, viewStore: ViewStore<AppState, AppAction>) -> some View {
        return HStack(alignment: .center, spacing: 0) {
            ForEach(0..<11) { column in
                cell(for: Location(row, column), viewStore: viewStore)
            }
        }
    }
}


