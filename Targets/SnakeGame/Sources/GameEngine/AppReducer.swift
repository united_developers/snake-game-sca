import Foundation
import ComposableArchitecture

typealias SnakeReducer = Reducer<AppState, AppAction, SnakeEnvironment>

extension SnakeReducer {
    struct TimerId: Hashable {}
    
    static var main = Reducer { (state: inout AppState, action: AppAction, environment: SnakeEnvironment) in
        switch action {
        case .changeCurrent(let direction):
            guard !state.snake.currentDirection.isOppositeOf(direction) else {
                return .none
            }
            state.snake.currentDirection = direction
            return .none
        case .startGame:
            state.alertState = nil
            state.snake = Snake(head: Location(5, 5), of: 5)
            if state.snake.allPositions.contains(state.mouse) {
                state.mouse = AppState.randomMouse(for: state.snake, generator: environment.generator)
            }
            return environment
                .timer(TimerId(), 0.3, UIScheduler.shared.eraseToAnyScheduler())
                .map { _ in .move }
        case .move:
            let direction = state.snake.currentDirection
            let headLocation = state.snake.headLocation(in: direction)
            
            guard !state.snake.allPositions.contains(headLocation) else {
                state.alertState = AlertState(title: TextState(verbatim: String("Game Over")),
                                              message: TextState(verbatim: String("Your snake is \(state.snake.allPositions.count) units long")),
                                              dismissButton: AlertState<AppAction>.Button.default(TextState(verbatim: String("OK"))))
                return .cancel(id: TimerId())
            }
            
            let hasEaten = state.mouse == headLocation
            let snake = state.snake.moving(direction: direction, hasEaten: { _ in state.mouse == headLocation })
            state.snake = snake
            if hasEaten {
                state.mouse = AppState.randomMouse(for: snake, generator: environment.generator)
            }
            return .none
        }
    }
}
