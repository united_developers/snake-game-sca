import Foundation
import ComposableArchitecture

typealias Mouse = Location

struct AppState: Equatable {
    var snake: Snake
    var mouse: Mouse
    var alertState: AlertState<AppAction>?
    
    init(snake: Snake, generator: @escaping (Range<Int>) -> Int = Int.random(in:)) {
        self.snake = snake
        self.mouse = Self.randomMouse(for: snake, generator: generator)
        self.alertState = nil
    }
    
    static func randomMouse(for snake: Snake, generator: @escaping (Range<Int>) -> Int = Int.random(in:)) -> Mouse {
        var row: Int
        var column: Int
        var mouse: Mouse
        
        repeat {
            row = generator(0..<11)
            column = generator(0..<11)
            mouse = Location(row, column)
        } while snake.allPositions.contains(mouse)
        return mouse
    }
}
