import SwiftUI
import ComposableArchitecture

@main
struct SnakeApp: App {
    let store: Store<AppState, AppAction> =
        .init(initialState: AppState(snake: Snake(head: Location(5, 5), of: 5)),
              reducer: SnakeReducer.main,
              environment: .live)
    
    var body: some Scene {
        WindowGroup {
            ContentView(store: store)
        }
    }
}
