import Foundation
import ComposableArchitecture

struct SnakeEnvironment {
    var generator: (Range<Int>) -> Int
    var timer: (_ id: AnyHashable,
                _ every: UIScheduler.SchedulerTimeType.Stride,
                _ scheduler: AnyScheduler<UIScheduler.SchedulerTimeType, UIScheduler.SchedulerOptions>) -> Effect<UIScheduler.SchedulerTimeType, Never>
}

extension SnakeEnvironment {
    static var live = Self(generator: Int.random, timer: { id, every, scheduler in
        return Effect.timer(id: id, every: every, on: scheduler)
    })
}

extension SnakeEnvironment {
    static var mock: SnakeEnvironment = .init(generator: { _ in return 0 },
                                              timer: { _, _, _ in fatalError("This dependency should not be invoked") })
    static func mock(with effect: Effect<UIScheduler.SchedulerTimeType, Never>) -> SnakeEnvironment {
        return .init(generator: { _ in return 0 },
                     timer: { _, _, _ in effect })
    }
}
