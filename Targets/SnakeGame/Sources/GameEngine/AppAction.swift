import Foundation

enum AppAction: Equatable {
    case changeCurrent(Direction)
    case startGame
    case move
}
