import XCTest
import ComposableArchitecture
import Combine

class SnakeTests: XCTestCase {
    
    static var zeroGenerator: (Range<Int>) -> Int = { _ in 0 }

    func test_ChangeDirection() throws {
        let snake = Snake(head: Location(5, 5), of: 5)
        let store = TestStore(initialState: AppState(snake: snake, generator: { _ in return 0 }),
                              reducer: SnakeReducer.main,
                              environment: .mock)
        
        store.assert(
            .send(.changeCurrent(.right)) {
                self.check(state: &$0, in: .right)
            },
            .send(.changeCurrent(.down)) {
                self.check(state: &$0, in: .down)
            },
            .send(.changeCurrent(.left)) {
                self.check(state: &$0, in: .left)
            },
            .send(.changeCurrent(.up)) {
                self.check(state: &$0, in: .up)
            }
        )
    }
    
    func test_moveAndCatch() throws {
        let snake = Snake(head: Location(5, 5), of: 5)
        var index = 0
        let positionToGenerate = [5, 6]
        
        let store = TestStore(initialState:
                                AppState(snake: snake,
                                         generator: { _ in
                                            let position = positionToGenerate[index]
                                            index += 1
                                            return position
                                         }),
                              reducer: SnakeReducer.main,
                              environment: .mock)
        store.assert(
            .send(.move) { state in
                state.snake.headPosition = Location(5, 6)
                state.snake.tailPosition.insert(Location(5, 5), at: 0)
                state.snake.currentDirection = .right
                state.mouse = Location(0, 0)
            }
        )
    }
    
    func test_GameFlow() throws {
        let subject = PassthroughSubject<UIScheduler.SchedulerTimeType, Never>()
        let snake = Snake(head: Location(5, 5), of: 5)
        let store = TestStore(initialState: AppState(snake: snake, generator: Self.zeroGenerator),
                              reducer: SnakeReducer.main,
                              environment: .mock(with: subject.eraseToEffect()))
        
        store.assert(
            .send(.startGame) { state in
                state.alertState = nil
                state.snake.headPosition = Location(5, 5)
                state.snake.tailPosition = [Location(5, 4), Location(5, 3), Location(5, 2), Location(5, 1)]
                state.mouse = Location(0, 0)
            },
            .send(.changeCurrent(.up)) { state in
                state.snake.currentDirection = .up
            },
            .do {
                subject.send(.init(.now()))
            },
            .receive(.move) { state in
                state.alertState = nil
                state.snake.headPosition = Location(4, 5)
                state.snake.tailPosition = [Location(5, 5), Location(5, 4), Location(5, 3), Location(5, 2)]
                state.mouse = Location(0, 0)
            },
            .send(.changeCurrent(.left)) { state in
                state.snake.currentDirection = .left
            },
            .do {
                subject.send(.init(.now()))
            },
            .receive(.move) { state in
                state.alertState = nil
                state.snake.headPosition = Location(4, 4)
                state.snake.tailPosition = [Location(4, 5), Location(5, 5), Location(5, 4), Location(5, 3)]
                state.mouse = Location(0, 0)
            },
            .send(.changeCurrent(.down)) { state in
                state.snake.currentDirection = .down
            },
            .do {
                subject.send(.init(.now()))
            },
            .receive(.move) { state in
                state.alertState = AlertState(title: TextState(verbatim: String("Game Over")),
                                              message: TextState(verbatim: String("Your snake is 5 units long")),
                                              dismissButton: AlertState<AppAction>.Button.default(TextState(verbatim: String("OK"))))
            },
            .do {
                subject.send(completion: .finished)
            }
        )
    }
    
    func check(state: inout AppState, in direction: Direction, headLocation: Location = Location(5, 5)) {
        state.snake.currentDirection = direction
        state.snake.headPosition = headLocation
        state.alertState = nil
        state.mouse = Location(0, 0)
    }
}
