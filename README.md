## Snake Game

* Simple snake game
* SwiftUI + Combine
* Swift Composable Architecture (Redux-like/UDF)
* Unit Tests
* iOS 14+

### SnakeGame Project is managed by Tuist Tool

You can follow the steps below to get things done. Or just run the script from the root of the project.

Make sure you have *curl* and *bash* installed. 

```shell
./setup_project.sh
```

## What's Tuist 🕺

Tuist is a command line tool that helps you **generate**, **maintain** and **interact** with Xcode projects.

It's open source and written in Swift.

## Install ⬇️

### Running script (Recommended)

```shell
bash <(curl -Ls https://install.tuist.io)
```

## Bootstrap your first project 🌀

```bash
tuist init --platform ios
tuist generate # Generates Xcode project & workspace
tuist build # Builds your project
```
# or just generate and open it if you have one

```bash
tuist generate --open
```
[Check out](https://docs.tuist.io) the project "Get Started" guide to learn more about Tuist and all its features.

## 
