#!/bin/sh

BASH=`which bash`
TUIST=`which tuist`
CURL=`which curl`

$CURL -Ls https://install.tuist.io | $BASH; $TUIST generate --open
